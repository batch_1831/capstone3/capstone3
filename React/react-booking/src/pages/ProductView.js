import { useState, useEffect, useContext, Navigate } from 'react';
import { Link, useParams, useNavigate } from 'react-router-dom';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function ProductView() {

    const {user} = useContext(UserContext);

    const navigate = useNavigate();

    const {productId} = useParams();


    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [stocks, setStocks] = useState(0);
    const [price, setPrice] = useState(0);
    const [quantity, setQuantity] = useState(1);
    const [subTotal, setSubTotal] = useState(0);

    const addToCart = (productId) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/cart`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                name: name,
                productId: productId,
                quantity: quantity,
                subTotal: subTotal
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(data){
                
                Swal.fire({
                    title: 'Added to cart',
                    icon: 'success',
                    text: 'You have successfully added this item to your cart'
                })
                navigate('/');
            }
            else{
                Swal.fire({
                    title: 'Something went wrong',
                    icon: 'error',
                    text: 'Please try again.'
                    })
                }
            })
        }


    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
        .then(res => res.json())
        .then(data => {
            setName(data.name);
            setDescription(data.description);
            setStocks(data.stocks);
            setPrice(data.price);

            

        })


    }, [productId])

    useEffect(() => {
        setSubTotal(quantity * price);
    }, [quantity, price])

    const increment = (quantity) => {
        if(quantity >= 1){
            setQuantity(quantity + 1);
        }
        
    }

    const decrement = (quantity) => {
        if(quantity  <= 1 ){
            alert("You can't have quantity below 1");
        }
        else{
            setQuantity(quantity - 1);
        }
    }

    

    return(
        (user.isAdmin)
        ?
            <Navigate to='/admin' />
        :
        <>
            <Container className="mt-5 productView">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card className='border-light'>
						<Card.Body className="text-center productView">
							<Card.Title className='card-header mb-2'>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}
                            </Card.Text>

                            <Card.Subtitle>Stocks:</Card.Subtitle>
                            <Card.Text>{stocks}</Card.Text>

							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>

                            <Card.Subtitle className='mb-2'>Quantity:</Card.Subtitle>

							<Card.Text>
                            <Button className='btn-secondary text-light plusM me-3 quantityButton glyphicon-minus' onClick={() => decrement(quantity)}>&#x2212;</Button>
                            {quantity}
                            <Button className='btn-secondary text-light plusM ms-3 quantityButton' onClick={() => increment(quantity)}>&#x2b;</Button>
                            </Card.Text>

                            <Card.Subtitle>Php</Card.Subtitle>
							<Card.Text>{subTotal}</Card.Text>

		
							<div className='d-grid gap-2 m-3'>

                            {
                                (user.id !== null)
                                ?
                                    <Button variant='danger'  size="lg" onClick={() => addToCart(productId)}>Add to cart</Button>
                                :
                                    <Button as={Link} to='/login' variant="info"  size="lg">Login to add to cart</Button>

                            }
                            
                            </div>

						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
        </>
    )
};