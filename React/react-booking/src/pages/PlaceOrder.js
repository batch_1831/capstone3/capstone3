import { useEffect, useState, useContext } from 'react';
import { Table, Button, Container, Nav, Form } from 'react-bootstrap'
import { Link, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function PlaceOrder(){

    const {user} = useContext(UserContext);
    const [cartItems, setCartItems] = useState([]);
    const [total, setTotal] = useState(0)
    const [haveCart, setHaveCart] = useState(true)
    const [finalPrice, setFinalPrice] = useState(0)
    const navigate = useNavigate();


    const fetchCart = () => {
        fetch(`${process.env.REACT_APP_API_URL}/users/userCart`, {
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if(data === false){
                setHaveCart(false);
            }
            else{
                setHaveCart(true);

                

                setCartItems(data.map(item => {

                    
                    if(data.length > 0){

                        return(
                            <tr key={item._id} className='text-center'>
                                <td>{item.name}</td>
                                <td>{item.price}</td>
                                <td>{item.quantity}
                                </td>
                                <td>{item.subTotal}</td>
                            </tr>
                        )
                    }
                    else{
                        return(
                            setHaveCart(false)
                        )
                    }

                }))
            }
            
            
        })
    }

    const transferOrder = () => {
        fetch(`${process.env.REACT_APP_API_URL}/users/userOrders`, {
            method: "POST",
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data){
                Swal.fire({
                    title: "Successfully placed your orders",
                    icon: "success"
                })
                navigate('/orders')
            }
            else{
                Swal.fire({
                    title: "Error",
                    icon: "error",
                    text: "Please try again"
                })
            }
        })
    }

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/users/cartTotal`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if(!data){
                return false;
            }
            else{
                setFinalPrice(data)
            }

        })
    }, [cartItems])


    useEffect(() => {
        fetchCart()
    }, [cartItems])
    
    
    useEffect(() => {
       fetchCart()

    }, [])

    

    
    return(
        (user.id == null)
        ?
            <>
                <Container>
                    <h1 className='d-flex justify-content-center align-items-center my-3'>Place Order</h1>
                    <p className='d-flex justify-content-center align-items-center my-3'><Nav.Link as={Link} to='/register' className='d-inline-block text-success px-1'>Create an account</Nav.Link> first or <Nav.Link as={Link} to='/login' className='d-inline-block text-success px-1'>log in</Nav.Link> to your account</p>
                </Container>
            </>
        :
        <>
            {
                (haveCart)
                ?
                <>
                <h1 className='d-flex justify-content-center align-items-center my-3'>Place Order</h1>
                <Container className='mt-3 d-flex justify-content-center align-items-center col-lg-12 tablefluid'>
                    <Table striped bordered hover>
                        <thead>
                          <tr className='text-center'>
                            <th>Product</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Subtotal</th>
                          </tr>
                        </thead>
                        <tbody>
                          {cartItems}
                        </tbody>
                    </Table>
                </Container>
                <div className='d-grid gap-2 secondary my-3 mx-5'>
                    <div className='ms-auto'>

                        <h3>Total: Php {finalPrice}</h3>
                    </div>
                    <Button className='' onClick={() => transferOrder()}>Place Order</Button>
                </div>
                </>
                
                :
                <Container className='text-center mt-3'>
                        <p>Nothing on you cart yet. Go to <Nav.Link as={Link} to='/products' className='d-inline-block text-success px-1'>Products</Nav.Link> to view our available items</p>
                </Container>
            }

        </>
    );
}