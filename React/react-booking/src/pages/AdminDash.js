import { Table, Button, Modal, Form, Row, Container } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Navigate, Link, useNavigate } from 'react-router-dom'

import UserContext from '../UserContext';
import Swal from 'sweetalert2';



export default function AdminDash(){

    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => {setShow(true)
    };

    const [show1, setShow1] = useState(false);
    const handleClose1 = () => setShow1(false);
    const handleShow1 = () => {setShow1(true)
    };
    
    const [itemUpdate, setItemUpdate] = useState();
    const {user} = useContext(UserContext);
    const [allItems, setAllItems] = useState([]);

    const [name, setName] = useState("");
    const [itemId, setItemId] = useState("")
    const [description, setDescription] = useState("")
    const [stocks, setStocks] = useState(0);
    const [onSale, setOnSale] = useState(true);
    const [percentOff, setPercentOff] = useState(0);
    const [price, setPrice] = useState(0);
    const [isOnSale, setIsOnSale] = useState(false)
    const navigate = useNavigate();
    const [isActive, setIsActive] = useState(false);

    const [name1, setName1] = useState("");
    const [description1, setDescription1] = useState("");
    const [price1, setPrice1] = useState(0);
    const [stocks1, setStocks1] = useState(0);
    const [category1, setCategory1] = useState("")

    function openCreateProduct() {
        handleShow1()
    }

    function createProduct(e) {
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/products/addProduct`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name1,
                description: description1,
                price: price1,
                stocks: stocks1,
                category: category1
            })
        })
        .then(res => res.json())
        .then(data => {
           if(data){
            setName1("");
            setDescription1("");
            setCategory1("");
            setStocks1(0);
            setPrice1(0);
                Swal.fire({
                    title: "Successfully Added",
                    icon: "success",
                    text: "A new product was added!"
                })
                handleClose1()
                navigate("/admin")
           }
           else{
                Swal.fire({
                    title: "Adding failed",
                    icon: "error",
                    text: "Failed to add a product. Please try again"
                })
           }
        })
    }

    useEffect(()=>{
		if(name1 !== "" && description1 !== "" && price1 > 0 && stocks1 > 0 && category1 !== ""){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	},[name1, description1, price1, stocks1, category1])





    const fetchItems = () => {
        fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}` 
            }
        })
        .then(res => res.json())
        .then(data => {
            setAllItems(data.map(item => {
                return (
                    <tr key={item._id}>
                        <td>{item._id}</td>
                        <td>{item.name}</td>
                        <td>{item.description}</td>
                        <td>{item.stocks}</td>
                        <td>{item.onSale ? "Sale" : ""}</td>
                        <td>{item.percentOff}</td>
                        <td>{item.isActive ? "Active" : "Inactive"}</td>

                        <td>
                        {
                            (item.isActive)
                            ?
                                <Button className='mt-1' variant='danger' size='sm' onClick={() => archive(item._id, item.name)}>Archive</Button>
                            :
                                <>
                                <Button className='my-1'variant='success' size='sm' onClick={() => unarchive(item._id, item.name)}>Unarchive</Button>

                                <Button variant='secondary' size='sm' onClick={() => edit(item._id)}>Edit</Button>

                                </>
                        }
                        </td>
                    </tr>
                )
            }))
        })
    }

    const archive = (itemId, itemName) => {

        fetch(`${process.env.REACT_APP_API_URL}/products/${itemId}/archive`, {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                isActive: false
            })

        })
        .then(res => res.json())
        .then(data => {
            // console.log(data)
            if(data){
                Swal.fire({
                    title: "Archived Item",
                    icon: "success",
                    text: `${itemName} is now inactive`
                })
                fetchItems();
            }
            else{
                Swal.fire({
                    title: "Archived item unsuccessful",
                    icon: "error",
                    text: `Something went wrong. Please try again later`
                })
            }
        })
    }

    const unarchive = (itemId, itemName) => {

        fetch(`${process.env.REACT_APP_API_URL}/products/${itemId}/archive`, {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                isActive: true
            })

        })
        .then(res => res.json())
        .then(data => {
            // console.log(itemUpdate)
            if(data){
                Swal.fire({
                    title: "Unarchived Item",
                    icon: "success",
                    text: `${itemName} is now active`
                })
                fetchItems();
            }
            else{
                Swal.fire({
                    title: "Unarchived item unsuccessful",
                    icon: "error",
                    text: `Something went wrong. Please try again later`
                })
            }
        })
    }



    const edit = (itemId) => {

        fetch(`${process.env.REACT_APP_API_URL}/products/${itemId}`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setItemId(data._id);
            setName(data.name);
            // console.log(name)
            setDescription(data.description);
            setPrice(data.price);
            setStocks(data.stocks);
            setPercentOff(data.percentOff)
            setOnSale(onSale);
           });

        handleShow()


    }

    const update = (e, itemId) => {
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/products/${itemId}/update`, {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                stocks: stocks,
                onSale: onSale,
                price: price,
                percentOff: percentOff
            })
        })
        .then(res => res.json())
        .then(data => {
            // console.log(data)

            if(data){
                Swal.fire({
                    title: "Successfully updated!",
                    icon: "success",
                    text: ""
                })
                fetchItems()
            }
        })


        handleClose()
    }
    
    useEffect(() => {
        fetchItems()

    }, [])

    const sale = () => {
        setIsOnSale(true)
    }
    const notSale  = () => {
        setIsOnSale(false)
    }

    return(
        (user.isAdmin)
        ?
        <>
            <div className='mt-3 mb-3 text-center'>
                <h1>Admin Dashboard</h1>
                <Button variant='primary' size='lg' className='m-1' onClick={() => openCreateProduct()}>Add Product</Button>
                <Button variant='success' size='lg' className='mx-1' as={Link} to="/showOrders">Show Orders</Button>
            </div>
            <Container fluid>
                <Table striped bordered hover className='text-center table-active'>
                    <thead>
                      <tr>
                        <th>Product ID</th>
                        <th>Product Name</th>
                        <th>Description</th>
                        <th>Stocks</th>
                        <th>Sale</th>
                        <th>PercentOff</th>
                        <th>Status</th>
                        <th>Action</th>

                      </tr>
                    </thead>
                    <tbody>
                     {allItems}
                    </tbody>
                </Table>

            </Container>

            <Modal dialogClassName='my-modal' show={show} onHide={handleClose}>
                    <Modal.Header closeButton>
                      <Modal.Title>Edit details</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                    <Form onSubmit={(e) => update(e, itemId)}>
            
                        <Form.Group className='mb-2' controlId="name">
                          <Form.Label>Product Name</Form.Label>
                          <Form.Control type="text" value={name} onChange={e => setName(e.target.value)}/>
                        </Form.Group>

                        <Form.Group className='mb-2' controlId="description">
                          <Form.Label>Description</Form.Label>
                          <Form.Control type="text" value={description} as='textarea' rows={3} onChange={e => setDescription(e.target.value)}/>
                        </Form.Group>

                        <Form.Group className='mb-2' controlId="stocks">
                          <Form.Label>Stocks</Form.Label>
                          <Form.Control type="number" value={stocks}  onChange={e => setStocks(e.target.value)}/>
                        </Form.Group>

                        <Form.Group className='mb-2' controlId="price">
                          <Form.Label>Price</Form.Label>
                          <Form.Control type="number" value={price}  onChange={e => setPrice(e.target.value)}/>
                        </Form.Group>

                        <Form.Group className='mb-2' controlId="onSale">

                                <Form.Label>Sale</Form.Label>
                                <div className='d-flex'>
                                
                                <Form.Check
                                 type="radio" name="optionsRadios"id="optionsRadios1" value={onSale} onClick={() => sale()}
                                  />
                                  <p className='ms-1'>On Sale</p>
                                </div>

                                <div className='d-flex'>
                                <Form.Check 
                                 type="radio" name="optionsRadios" id="optionsRadios2" value={onSale} onClick={() => notSale()}
                                  /><p className='ms-1'>No</p>

                                </div>
                        </Form.Group>

                        {
                            isOnSale
                            ?
                            <Form.Group className='mb-2' controlId="percentOff">
                                <Form.Label>Percent Off</Form.Label>
                                <Form.Control type="number" value={percentOff} onChange={e => setPercentOff(e.target.value)}/>
                            </Form.Group>
                            :
                            <Form.Group className='mb-2' controlId="percentOff">
                                <Form.Label>Percent Off</Form.Label>
                                <Form.Control type="number" value={percentOff} disabled onChange={e => setPercentOff(e.target.value)}/>
                            </Form.Group>

                        }

                        <div className='d-grid gap-2 mt-4'>
                            <Button variant="primary" type='submit' id= "submitBtn">
                                Save Changes
                            </Button>
                        </div>
        
                    </Form>
                        
                    </Modal.Body>
                    

                    <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Close
                    </Button>
                    
                    </Modal.Footer>
                </Modal>

{/* ----------------------------------- */}

                <Modal dialogClassName='my-modal' show={show1} onHide={handleClose1}>
                    <Modal.Header closeButton>
                      <Modal.Title>Create a New Product</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                    <Form onSubmit = {(e) => createProduct(e)} className='mx-1'>

					    <Form.Group className="mb-2" controlId="name">
					      <Form.Label>Name of the product</Form.Label>
					      <Form.Control type="text" placeholder="Product Name" value={name1} onChange={e => setName1(e.target.value)}/>
					    </Form.Group>

					    <Form.Group className="mb-2" controlId="description">
					      <Form.Label>Description</Form.Label>
					      <Form.Control type="text" as='textarea' rows={3} placeholder="Describe your new product" value={description1} onChange={e => setDescription1(e.target.value)}/>
					    </Form.Group>

					    <Form.Group className="mb-2" controlId="price">
					      <Form.Label>Price</Form.Label>
					      <Form.Control type="number" placeholder="Hoow much is your product" value={price1} onChange={e => setPrice1(e.target.value)}/>
					    </Form.Group>

					    <Form.Group className="mb-2" controlId="stocks">
					      <Form.Label>Stocks</Form.Label>
					      <Form.Control type="number" placeholder="Stocks" value={stocks1} onChange={e => setStocks1(e.target.value)}/>
					    </Form.Group>

				          <Form.Group className="mb-2" controlId="category">
				            <Form.Label>Category</Form.Label>
				            <Form.Control type="text" placeholder="Category" value={category1} onChange={e => setCategory1(e.target.value)}/>
				          </Form.Group>

		    	        <div className="d-grid gap-2 mt-3">
				        {
		    	        	isActive
		    	        	?
		    	        		<Button className="btn-success btn-lg d-grid gap-2" type="submit" id="submitBtn">
		    	        		  Create
		    	        		</Button>
		    	        	:
		    	        		<Button className="btn-success btn-lg" type="submit" id="submitBtn" disabled>
		    	        		  Create
		    	        		</Button>
		    	        }
				        </div>
		    	    </Form>
                        
                    </Modal.Body>
                    

                    <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose1}>
                        Close
                    </Button>
                    
                    </Modal.Footer>
                </Modal>


            
        </>
        :
            <Navigate to='/products' />
    )
}

