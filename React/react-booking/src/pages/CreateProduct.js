import { Container, Button, Form  } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext";
import { Navigate, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";


export default function CreateProduct(){

    const {user} = useContext(UserContext);
    const navigate = useNavigate();
    const [isActive, setIsActive] = useState(false);

    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    const [stocks, setStocks] = useState(0);
    const [category, setCategory] = useState("")

    function createProduct(e) {
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/products/addProduct`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price,
                stocks: stocks,
                category: category
            })
        })
        .then(res => res.json())
        .then(data => {
           if(data){
                Swal.fire({
                    title: "Successfully Added",
                    icon: "success",
                    text: "A new product was added!"
                })
                navigate("/admin")
           }
           else{
                Swal.fire({
                    title: "Adding failed",
                    icon: "error",
                    text: "Failed to add a product. Please try again"
                })
           }
        })
    }

	useEffect(()=>{
		if(name !== "" && description !== "" && price > 0 && stocks > 0 && category !== ""){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	},[name, description, price, stocks, category])

    return (
        (user.isAdmin)
		?
		<>
			<Container  fluid className='col-lg-6 col-md-6'>
				<h1 className="mb-3 mt-4 text-center">Create Product</h1>		
				<Form onSubmit = {(e) => createProduct(e)} className='mt-3 mb-2'>

					<Form.Group className="mb-2" controlId="name">
					  <Form.Label>Name of the product</Form.Label>
					  <Form.Control type="text" placeholder="Product Name" value={name} onChange={e => setName(e.target.value)}/>
					</Form.Group>

					<Form.Group className="mb-2" controlId="description">
					  <Form.Label>Description</Form.Label>
					  <Form.Control type="text" placeholder="Describe your new product" value={description} onChange={e => setDescription(e.target.value)}/>
					</Form.Group>

					<Form.Group className="mb-2" controlId="price">
					  <Form.Label>Price</Form.Label>
					  <Form.Control type="number" placeholder="Hoow much is your product" value={price} onChange={e => setPrice(e.target.value)}/>
					</Form.Group>

					<Form.Group className="mb-2" controlId="stocks">
					  <Form.Label>Stocks</Form.Label>
					  <Form.Control type="number" placeholder="Stocks" value={stocks} onChange={e => setStocks(e.target.value)}/>
					</Form.Group>

				      <Form.Group className="mb-2" controlId="category">
				        <Form.Label>Category</Form.Label>
				        <Form.Control type="text" placeholder="Category" value={category} onChange={e => setCategory(e.target.value)}/>
				      </Form.Group>

		    	  <div className="d-grid gap-2 mt-4 mb-5">
				  {
		    	  	isActive
		    	  	?
		    	  		<Button className="btn-success btn-lg d-grid gap-2" type="submit" id="submitBtn">
		    	  		  Submit
		    	  		</Button>
		    	  	:
		    	  		<Button className="btn-success btn-lg" type="submit" id="submitBtn" disabled>
		    	  		  Submit
		    	  		</Button>
		    	  }
				  </div>
		    	</Form>
			</Container>
		</>
        :
        <Navigate to="/products"/>
		
    )
}