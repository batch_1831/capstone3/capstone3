import { Carousel, Container, Row, Col, Form } from "react-bootstrap";
import img1 from './img1.jpeg';
import img2 from './img2.jpeg';
import img3 from './img3.jpeg';

export default function Banner(){




    return(
      <>
        {/* <Container>
        <div class="form-check form-switch">
        <Form.Control type="checkbox" id="flexSwitchCheckDefault" />

        <label class="form-check-label" for="flexSwitchCheckDefault">Default switch checkbox input</label>
      </div>
        </Container> */}
        <Container className='mt-3'>
          <Row className='justify-content-center'>
            <Col md='11'>
            <Carousel fade>
              <Carousel.Item>
                <img
                  className="d-block w-100 roundCarousel"
                  src={img1}
                  alt="First slide"
                />
              </Carousel.Item>

              <Carousel.Item>
                <img
                  className="d-block w-100 roundCarousel"
                  src={img2}
                  alt="Second slide"
                />
              </Carousel.Item>

              <Carousel.Item>
                <img
                  className="d-block w-100 roundCarousel"
                  src={img3}
                  alt="Third slide"
                />
              </Carousel.Item>

            </Carousel>
            </Col>
          </Row>
        </Container>

        <Container className="d-flex align-items-center justify-content-center text-center">
          <Col xs='12' md='10'  className='my-5'>
              <h1 className="text-align-center">Hot Products</h1>
          </Col>
            
        </Container>

        </>
    )
}