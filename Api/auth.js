const jwt = require("jsonwebtoken");

const secret = "encryptedMessage";

module.exports.accessToken = (user) => {

    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin,
        totalOrderAmount: user.totalOrderAmount
    }

    return jwt.sign(data, secret, {});
};

module.exports.verify = (req, res, next) => {
    let token = req.headers.authorization;

    if(typeof token !== "undefined"){
        token = token.slice(7, token.length);

        return jwt.verify(token, secret, (err, data) => {
            if(err){
                return res.send("You need to register first")
            }
            else{
                next();
            }
        })
    }
    else{
        return res.send("You need to register first");
    };
    
};

module.exports.decode = (token) => {
    if(typeof token != "undefined"){
        token = token.slice(7, token.length);

        return jwt.verify(token, secret, (err, data) => {
            if(err){
                return null;
            }
            else{
                return jwt.decode(token, {complete: true}).payload;
            };
        });
    }
    else{
        return null;
    };
};