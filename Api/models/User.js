const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, "First name is required"]
    },
     lastName: {
        type: String,
        required: [true, "Last name is required"]
    },
    userName:{
        type: String,
        required: [true, "Username is required"]
    },
    email: {
        type: String,
        required: [true, "Email is required"]
    },
    password: {
        type: String,
        required: [true, "Password is required"]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    mobileNo: {
        type: String,
        required: [true, "Mobile number is requied"]
    },
    createdOn:{
        type: Date,
        default: new Date()
    },
    cart:[
        {
        productId: {
            type: String,
            required: [true, "Product Id is required"]
        },
        price: {
            type: Number,
            required: [true, "product price is required"]
        },
        name: {
            type: String, 
            required: [true, "Name is required"]
        },
        subTotal: {
            type: Number,
            default: 0
        },
        quantity: {
            type: Number,
            default: 1
        }

    }],
    orders: [
        {
        name: {
            type: String, 
            required: [true, "Name is required"]
        },
        productId: {
            type: String,
            required: [true, "Product Id is required"]
        },
        quantity: {
            type: Number,
            default: 1
        },
        orderedOn: {
            type: Date,
            default: new Date()
        }, 
        status: {
            type: String,
            default: "Pending..."
        }
    }],
    totalOrderAmount: {
        type: Number,
        default: 0
    }
});

module.exports = mongoose.model("User", userSchema);