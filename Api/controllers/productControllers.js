const Product = require("../models/Product");
const objectId = require("mongodb").ObjectId;

module.exports.addProduct = (reqBody) => {
    let newProduct = new Product({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price,
        stocks: reqBody.stocks,
        category: reqBody.category
    })

    return newProduct.save().then((success, err) => {
        if(err){
            return false;
        }
        else{
            return true;
        }
    })
};

module.exports.sale = () => {
    return Product.find({$and: [{"onSale": true}, {"isActive": true}]}, {"createdOn": 0, "usersOrder": 0, "isActive": 0, "__v": 0}).then(result => result);

};

module.exports.getAll = () => {
    return Product.find({}).then(result => result)
}

module.exports.getActive = () => {
    return Product.aggregate([{$match: {"isActive": true}}, {$project: {"createdOn": 0, "usersOrder": 0, "__v": 0}}]).then(result => result);
};

module.exports.search = (reqBody) => {
    return Product.find({$and: [{"name": {$regex: reqBody.name, $options: "$i"}}, {"isActive": true}]}, {"createdOn": 0, "usersOrder": 0, "isActive": 0, "__v": 0}).then(result => result);
};

module.exports.searchCategory = (reqBody) => {
    return Product.find({$and: [{"category": {$regex: reqBody.category, $options: "$i"}}, {"isActive": true}]}, {"createdOn": 0, "usersOrder": 0, "isActive": 0,  "__v": 0}).then(result => result);
};

module.exports.getProduct = (productId) => {
    return Product.findById(productId).then(result => result)
};

module.exports.updateProduct = (productId, reqBody) => {
    let updateProduct = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price,
        stocks: reqBody.stocks,
        onSale: reqBody.onSale,
        percentOff: reqBody.percentOff
    }

    return Product.findByIdAndUpdate(productId, updateProduct).then((success, err) => {
        if(err){
            return false;
        }
        else{
            return true;
        }
    })
};


module.exports.archiveProduct = (productId, reqBody) => {
    let archive = {
        isActive: reqBody.isActive
    }

    return Product.findByIdAndUpdate(productId, archive).then((success, err) => {
        if(err){
            return false;
        }
        else{
            return true;
        }
    })
};


