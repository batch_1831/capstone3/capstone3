const User  = require("../models/User");
const Product = require("../models/Product");
const objectId = require("mongodb").ObjectId;

const bcrypt = require("bcrypt");
const auth = require("../auth");


module.exports.signUp = (reqBody) =>{
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName: reqBody.lastName,
        userName: reqBody.userName,
		email: reqBody.email,
		// Syntax: bcrypt.hashSync(dataToBeEncrypted, salt)
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo
	})

	return newUser.save().then((user, error) =>{
		if(error){
			return false;
		}
		else{
			// console.log(user);
			return true;
		}
	})
}

module.exports.checkUserName = (reqBody) => {
	return User.find({userName: reqBody.userName}).then(result => {
		if(result == null || result.length == 0 || result == []){
			return false;
		}

		else{
			return true;
		}
	});
}

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0){
			return true;
		}

		else{
			return false;
		}
	});
}

module.exports.decrementItemQuantity = async (userId, productId) => {

    let change = {
        subTotal: 0
    }


    let product = await Product.findById(productId).then(result => result)

    let user = await User.findById(userId).then(result => {
        result.cart.map((item) => {

            let percent = product.percentOff / 100;

            if(item.productId == productId){
                
                if(product.stocks >= item.quantity && item.quantity !== 1){
                    item.quantity = item.quantity - 1;

                    if(product.onSale === true){
                        change.subTotal = (item.price - (percent * item.price)) * item.quantity;
    
                        item.subTotal = change.subTotal

                    return result.save().then((success, err) => {
                        if(err){
                            return false;
                        }
                        else{
                            return true;
                        }
                    })    
                    
                    }

                    else{
                        change.subTotal = item.quantity * item.price;
    
                        item.subTotal = change.subTotal
    
                        return result.save().then((success, err) => {
                        if(err){
                            return false;
                        }
                        else{
                            return true;
                        }
                    })   
                    }        
                }
                else{
                    return false;
                }
                
                
            }
            else{
                return false;
            }

        })
    })
    if(user && product){
        return true;
    }

    else{
        return false;
    }

}

module.exports.incrementItemQuantity = async (userId, productId) => {

    let change = {
        subTotal: 0
    }


    let product = await Product.findById(productId).then(result => result)

    let user = await User.findById(userId).then(result => {
        result.cart.map((item) => {
            let percent = product.percentOff / 100;

            if(item.productId == productId){
                
                if(product.stocks >= item.quantity){
                    item.quantity = item.quantity + 1;
                    console.log(item.quantity)
                    if(product.onSale === true){
                        change.subTotal = (item.price - (percent * item.price)) * item.quantity;
    
                        item.subTotal = change.subTotal
                        item.price = product.price

                    return result.save().then((success, err) => {
                        if(err){
                            return false;
                        }
                        else{
                            return true;
                        }
                    })    
                    
                    }

                    else{
                        change.subTotal = item.quantity * item.price;
    
                        item.subTotal = change.subTotal
    
                        return result.save().then((success, err) => {
                        if(err){
                            return false;
                        }
                        else{
                            return true;
                        }
                    })   
                    }        
                }
                else{
                    return false;
                }
                
                
            }
            else{
                return false;
            }

        })
        // console.log(result)
    })

    
    if(user && product){
        return true;
    }

    else{
        return false;
    }

}

module.exports.login = (reqBody) => {
    return User.findOne({email: reqBody.email}).then(result => {
        if(result == null){
            return false
        }
        else{
            const passwordMatch = bcrypt.compareSync(reqBody.password, result.password);

            if(passwordMatch){
                return{access: auth.accessToken(result)};
            }
            else{
                return false
            }
        }
    })
};

module.exports.cartTotal = async (userId) => {

        return await User.findById(userId).then(result => {
            if(result.cart.length == 0){
                return false;
            }
            else{
            let total = [];
            let finalPrice = []

            result.cart.map((item) => {
            total.push(item.subTotal)



            
        })
        let final = total.reduce((a, b) => a + b);
            finalPrice.push(final)
            
            
            return finalPrice
            }
    })
};

module.exports.userCartItems = (userId) => {
    return User.findById(userId).then(result => {
       
            return result.cart;
        }
    )
};




// cart and price of products
module.exports.userCart = (userId) => {
    return User.findById(userId).then(result => {
        if(result.cart.length == 0){
            return false;
        }
        else{
            return result.cart;
        }
    })
}


module.exports.getProfile = (data) =>{
	// console.log(data)
	return User.findById(data.userId).then(result =>{
		result.password = ""

		return result;
	})
}


module.exports.updateStatusAdmin = (userId) => {

    return User.findById(userId).then(result => {
        if(result.isAdmin){
            result.isAdmin = false;
        }
        else{
            result.isAdmin = true;
        }
        return result.save().then((success, err) => {
            if(err){
                return false;
            }
            else{
                return true;
            }
        })
    })
};



module.exports.userOrders = async (userId) => {

    // let userCart = User.findById(userId).then(result => result.cart)

    // let user = User.findById(userId).then(result => result)

    

    // let check = await userCart.map((item) => {
        
    //     Product.findById(item.productId).then(result => {
    //         if(result.stocks >= item.quantity){

    //             result.stocks = result.stocks - item.quantity;



    //             total.push(item.subTotal);

    //             user.orders.push({name: item.name, productId: item.productId, quantity: item.quantity})

    //             return result.save().then((success, err) => {
    //                 if(err){
    //                     return false;
    //                 }
    //                 else{
    //                     return true;
    //                 }
    //             })
    //         }
    //         else{
    //             return false;
    //         }
    //     })
    // }) 

    // let totalAmount = await User.findById(userId).then(result => {

    //     let total = [];
    //     let finalPrice = [];

    //     let final = total.reduce((a, b) => a + b);
    //     finalPrice.push(final)

    //     result.totalOrderAmount = result.totalOrderAmount + finalPrice[0]

    //     result.cart = [];

        

    //     return result.save().then((success, err) => {
    //         if(err){
    //             return false;
    //         }
    //         else{
    //             return true;
    //         }
    //     })
    // })

    // if(check && totalAmount){
    //     return true;
    // }
    // else{
    //     return false;
    // }


    let cartItems = await User.findById(userId).then(result => result);

    let total = await User.findById(userId).then(result => {
        if(result.cart.length == 0){
            return false;
        }
        else{
        let total = [];
        let finalPrice = [];

        result.cart.map((item) => {
        total.push(item.subTotal)
        })
    
        let final = total.reduce((a, b) => a + b);
        finalPrice.push(final)
    
        
        
        return finalPrice
        }
    });

    let update = await User.findById(userId).then(result => {
        result.totalOrderAmount = result.totalOrderAmount + total[0];
        console.log(cartItems.totalOrderAmount)
        result.cart = [];

        return result.save().then((success, err) => {
            if(err){
                return false;
            }
            else{
                return true;
            }
        })
    })

    let order = await User.findById(userId).then(result => {
        cartItems.cart.map((item => {
            // console.log(item)
            result.orders.push({name: item.name,productId: item.productId, quantity: item.quantity})

        }))
            // console.log(cartItems)
        return result.save().then((success, err) => {
            if(err){
                return false;
            }
            else{
                return true;
            }
        })

    })

    if(cartItems && total && order && update){
        return true;
    }
    else{
        return false;
    }
    
};

module.exports.viewOrders =  (userId) => {
    return User.findById(userId).then(result => 
        result
    )
    
};

module.exports.getUserOrders = (userId) => {
    return User.findById(userId).then(result => result.orders)
}

module.exports.cart = async (data) => {
    let productPrice = await Product.findById(data.productId).then(product => {
        return product.price;
    });

    let onSaleOff = await Product.findById(data.productId).then(product => {
        if(product.onSale == true){
            return product.percentOff
        }
        else{
            return false;
        }
    });
    

    let product = await Product.findById(data.productId).then(product => product)


    let updateUser = await User.findById(data.userId).then(user => {

  

        if(product.onSale == true){


        data.subTotal = (productPrice * data.quantity) - ((productPrice * onSaleOff) / 100)
        
        user.cart.push({name: data.name, productId: data.productId, quantity: data.quantity, subTotal: data.subTotal, price: product.price});

        return user.save().then((success, err) => {
            if(err){
                return false;
            }
            else{
                return true;
            }
            })
        }
        else{
            data.subTotal = (productPrice * data.quantity);

            user.cart.push({name: data.name, productId: data.productId, quantity: data.quantity, subTotal: data.subTotal, price: product.price});

            return user.save().then((success, err) => {
                if(err){
                    return false;
                }
                else{
                    return true;
                }
                })
        }
    })

    if(updateUser){
        return true;
    }
    else{
        return false;
    }


}

module.exports.pendingOrder = async (orderId) => {

    let users = await User.find({isAdmin: false}).then(result => {
        result.map((user => {
            if(user.orders.length === 0){
                return false;
            }
            else{
                user.orders.map((order) => {
                    if(order._id == orderId){
                        order.status = "Pending..."

                    }

                    else{
                        return false;
                    }
                    
                    user.save().then((success, err) => {
                        if(err){
                            return false;
                        }
                        else{
                            return true;
                        }
                    })

                })
            }
        }))

    })

    if(users){
        return true;
    }
    else{
        return false;
    }
}


module.exports.completedOrder = async (orderId) => {

    let users = await User.find({isAdmin: false}).then(result => {
        result.map((user => {
            if(user.orders.length === 0){
                return false;
            }
            else{
                user.orders.map((order) => {
                    if(order._id == orderId){
                        order.status = "Completed"

                    }

                    else{
                        return false;
                    }
                    
                    user.save().then((success, err) => {
                        if(err){
                            return false;
                        }
                        else{
                            return true;
                        }
                    })

                })
            }
        }))

    })

    if(users){
        return true;
    }
    else{
        return false;
    }
}


module.exports.deleteOrder = async (orderId) => {
    let itemsRemove= [];

    let users = await User.find({isAdmin: false}).then(result => {
        result.map((user => {
            if(user.orders.length === 0){
                return false;
            }
            else{
                user.orders.map((order, index) => {
                    if(order._id == orderId){
                    itemsRemove.push(index)

                    for (i = itemsRemove.length -1; i >= 0; i--){

                    user.orders.splice(itemsRemove[i], 1);
                        }

                    }

                    else{
                        return false;
                    }
                    
                    user.save().then((success, err) => {
                        if(err){
                            return false;
                        }
                        else{
                            return true;
                        }
                    })

                })
            }
        }))

    })

    if(users){
        return true;
    }
    else{
        return false;
    }
}


module.exports.deleteProduct = async (userId, itemId) => {
    let itemsRemove = [];

    let newCart = await User.findById(userId).then(result => {
        result.cart.map((item, index) => {
            if(item.productId === itemId){

                itemsRemove.push(index)
                console.log(itemsRemove)

                for (i = itemsRemove.length -1; i >= 0; i--){
                    result.cart.splice(itemsRemove[i], 1);
                }
                
            }
            else{
                    return false;
                }

            result.save().then((success, err) => {
                    if(err){
                        return false;
                    }
                    else{
                        return true;
                    }
                })
            
            

            

        })
    }) 

    if(newCart){
        return true;
    }
    else{
        return false;
    }
        
}

module.exports.orders = async (data) => {

    let productPrice = await Product.findById(data.productId).then(product => {
        return product.price;
    });


    let onSale = await Product.findById(data.productId).then(product => product);

    let updateProduct = await Product.findById(data.productId).then(product => {
        
        if(product.stocks < data.quantity){
            return false;
        }

        else {
            product.stocks = product.stocks - data.quantity;
            product.usersOrder.push({userId: data.userId, quantity: data.quantity});

            User.findById(data.userId).then(user => {
                if(onSale.onSale == true){
                   
                    let initial = data.quantity * productPrice;

                    let percent = (onSale.percentOff / 100) * (data.quantity * productPrice);
                    
                    user.totalOrderAmount = user.totalOrderAmount + (initial - percent);
                    // console.log(user.totalOrderAmount);
                    return user.save().then((success, err) => {
                        if(err){
                            return false;
                        }
                        else{
                            return true;
                        }
                        })
                    }
                
                else{
                    user.totalOrderAmount = user.totalOrderAmount + (data.quantity * productPrice);
        
                    return user.save().then((success, err) => {
                    if(err){
                        return false;
                    }
                    else{
                        return true;
                    }
                    })
                }
            })
        }
        return product.save().then((success, err) => {
            if(err){
                return false;
            }
            else{
                return true;
            }
        })
    });

    let updateUser = await User.findById(data.userId).then(user => {

        if(updateProduct){
        user.orders.push({productId: data.productId, quantity: data.quantity});

        return user.save().then((success, err) => {
            if(err){
                return false;
            }
            else{
                return true;
            }
            })
        }
        else{
            return false;
        }
    })

    if(updateUser && updateProduct){
        return "Successfully placed an order"
    }
    else{
        return false
    }
};



module.exports.getAllUsersOrders = () => {
    return User.find({isAdmin: false}).then(result => {
        console.log(result)
    })

};


module.exports.allUsers = () => {
    return User.find({}).then(result => result)
}




module.exports.updateInfo = async (userId, reqBody) => {
    let updateInfo = {
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        userName: reqBody.userName,
        password: bcrypt.hashSync(reqBody.password, 10),
        mobileNo: reqBody.mobileNo
    }

    let users = User.find({}).then(result => result)

    let double = await users.map((user) => {
        if(user.userName === updateInfo.userName){
            return false;
        }

        else{
            return User.findByIdAndUpdate(userId, updateInfo).then((success, err) => {
                if(err){
                    return false;
                }
                else{
                    return true
                }
            })
        }
    })

    if(double){
        return true;
    }
    else{
        return false;
    }
};

