const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");
const auth = require("../auth");

router.post("/addProduct", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        productControllers.addProduct(req.body).then(result => res.send(result));
    }
    else{
        res.send("You don't have access on this page");
    };
});

router.get("/sale", (req, res) => {
    productControllers.sale().then(result => res.send(result));
});

router.get("/", (req, res) => {
    productControllers.getActive().then(result => res.send(result));
});

router.get("/all", auth.verify, (req, res) => {
    let userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        productControllers.getAll().then(resultFromControllers => res.send(resultFromControllers))
    }
});

router.get("/search", (req, res) => {
    productControllers.search(req.body).then(result => res.send(result));
});

router.get("/searchCategory", (req, res) => {
    productControllers.searchCategory(req.body).then(result => res.send(result));
});

router.get("/:productId", (req, res) => {
    productControllers.getProduct(req.params.productId).then(result => res.send(result));
});

router.patch("/:productId/update", auth.verify, (req, res) => {
    let userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        productControllers.updateProduct(req.params.productId, req.body).then(result => res.send(result));
    }
    else{
        res.send("You don't have access on this page")
    };
});

router.patch("/:productId/archive", auth.verify, (req, res) => {
    let userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        productControllers.archiveProduct(req.params.productId, req.body).then(result => res.send(result));
    }
    else{
        res.send("You don't have access on this page")
    }
});




module.exports = router;