const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers");
const auth = require("../auth");

router.post("/signup", (req, res) => {
    userControllers.signUp(req.body).then(result => res.send(result));
});

router.get("/:userId/userCartItems", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        userControllers.userCartItems(req.params.userId).then(result => res.send(result));
        
    }
    else{
        res.send('You dont have access on this page')
    }
});


router.post("/:productId/decrementQuantity", auth.verify, (req, res) => {
    let userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        res.send("You dont have permission on this page")
    }
    else{
        userControllers.decrementItemQuantity(userData.id, req.params.productId).then(result => res.send(result))
    }
})

router.post("/:productId/incrementQuantity", auth.verify, (req, res) => {
    let userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        res.send("You dont have permission on this page")
    }
    else{
        userControllers.incrementItemQuantity(userData.id, req.params.productId).then(result => res.send(result))
    }
});

router.get("/cartTotal", auth.verify, (req, res) => {
    let userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        res.send("You dont have permission on this page")
    }
    else{
        userControllers.cartTotal(userData.id).then(result => res.send(result))
    }
})

router.get("/userCart", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        res.send('You dont have access on this page')
    }
    else{
        userControllers.userCart(userData.id).then(result => res.send(result));
    }
});

// user change to admin

router.post('/:orderId/pending', auth.verify, (req, res) => {
    let userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        userControllers.pendingOrder(req.params.orderId).then(result => res.send(result));
    }
    else{
        res.send('You dont have access on this page')
    }
});

router.post('/:orderId/completed', auth.verify, (req, res) => {
    let userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        userControllers.completedOrder(req.params.orderId).then(result => res.send(result));
    }
    else{
        res.send('You dont have access on this page')
    }
});

router.delete('/:orderId/deleteOrder', auth.verify, (req, res) => {
    let userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        userControllers.deleteOrder(req.params.orderId).then(result => res.send(result));
    }
    else{
        res.send('You dont have access on this page')
    }
});

router.delete('/:itemId/deleteCart', auth.verify, (req, res) => {
    let userData = auth.decode(req.headers.authorization);

    userControllers.deleteProduct(userData.id, req.params.itemId).then(result => res.send(result));
});

router.get("/:userId/viewOrders", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);


    if(userData.isAdmin){
        userControllers.viewOrders(req.params.userId).then(result => res.send(result));
    }
    else{
        res.send("You don't have permission on this page")
        
    }
});

router.post("/userOrders", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        res.send("You don't have permission on this page")
    }
    else{
        userControllers.userOrders(userData.id).then(result => res.send(result));
    }
});

router.get("/getUserOrders", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        res.send("You don't have permission on this page")
    }
    else{
        userControllers.getUserOrders(userData.id).then(result => res.send(result));
    }
});

router.post("/checkEmail", (req, res) =>{
	userControllers.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/checkUser", (req, res) =>{
	userControllers.checkUserName(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/login", (req, res) => {
    userControllers.login(req.body).then(result => res.send(result));
});

router.get("/details", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization); 
    
	userControllers.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));
})

router.patch("/:userId/updateStatusAdmin", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        userControllers.updateStatusAdmin(req.params.userId).then(result => res.send(result));
    }
    else{
        res.send("You don't have the access on this page");
    }
});



router.post('/cart', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    let data = {
        userId: userData.id,
        name: req.body.name,
        productId: req.body.productId,
        quantity: req.body.quantity,
        subTotal: req.body.subTotal
    }

    if(userData.isAdmin){
        res.send("You dont have access on this page")
    }
    else{
        userControllers.cart(data).then(result => res.send(result));
    }
});

router.post("/orders", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    let data = {
        userId: userData.id,
        productId: req.body.productId,
        quantity: req.body.quantity
    }


    if(userData.isAdmin){
        res.send("You dont have access on this page");
    }
    else{
        userControllers.orders(data).then(result => res.send(result));
    }
});

// remove item from cart
// checkout


router.get("/allOrders", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        userControllers.getAllUsersOrders().then(result => res.send(result));
    }
    else{
        res.send ("You don't have access on this page");
    };
    
});


router.put("/info", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    let data = {
        userId: userData.id
    }

    if(userData.isAdmin){
        res.send("You don't have access on this page");
    }
    else{
        userControllers.updateInfo(data.userId, req.body).then(result => res.send(result));
    }
});

router.get("/allUsers", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        userControllers.allUsers().then(result => res.send(result));

    }
    else{
        res.send("You dont have access");
    }
})

module.exports = router;